package co.com.grupoasd.superhuman.model;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Entity modelo Catalogo.
 * 
 * @author Jean Khalo Lozano Ruiz
 * @version 2021/02/08
 */
@Entity
@Table(name = "sh_catalogo")
public class Catalogo {

	// Variables
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@NotNull
	@NotBlank
	@Length(min = 1, max = 10)
	private String codigo;

	@NotNull
	@NotBlank
	@Length(min = 1, max = 20)
	private String nombre;

	@NotNull
	@NotBlank
	@Length(max = 250)
	private String descripcion;

	@NotNull
	@OneToOne
	private TipoCatalogo tipoCatalogo;

	@JsonIgnore
	@ManyToMany(mappedBy = "poderes")
	private Set<SuperHumano> superHumano;

	/**
	 * Constructor.
	 */
	public Catalogo() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructor con parametros.
	 * 
	 * @param codigo
	 * @param nombre
	 * @param descripcion
	 * @param tipoCatalogo
	 * @param superHumano
	 */
	public Catalogo(@NotNull @NotBlank @Length(min = 1, max = 10) String codigo,
			@NotNull @NotBlank @Length(min = 1, max = 20) String nombre,
			@NotNull @NotBlank @Length(max = 250) String descripcion, @NotNull TipoCatalogo tipoCatalogo,
			Set<SuperHumano> superHumano) {
		super();
		this.codigo = codigo;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.tipoCatalogo = tipoCatalogo;
		this.superHumano = superHumano;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public TipoCatalogo getTipoCatalogo() {
		return tipoCatalogo;
	}

	public void setTipoCatalogo(TipoCatalogo tipoCatalogo) {
		this.tipoCatalogo = tipoCatalogo;
	}

	public Set<SuperHumano> getSuperHumano() {
		return superHumano;
	}

	public void setSuperHumano(Set<SuperHumano> superHumano) {
		this.superHumano = superHumano;
	}

}
