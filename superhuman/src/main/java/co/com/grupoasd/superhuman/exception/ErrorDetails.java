package co.com.grupoasd.superhuman.exception;

import java.util.Date;

/**
 * Clase DTO ErrorDetails para personalizar la salida de las excepciones.
 * 
 * @author Jean Khalo Lozano Ruiz
 * @version 2021/02/08
 */
public class ErrorDetails {

	// Variables
	private Date timestamp;
	private String message;
	private String details;

	/**
	 * Constructor con parametros.
	 * @param timestamp
	 * @param message
	 * @param details
	 */
	public ErrorDetails(Date timestamp, String message, String details) {
		super();
		this.timestamp = timestamp;
		this.message = message;
		this.details = details;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public String getMessage() {
		return message;
	}

	public String getDetails() {
		return details;
	}
}
