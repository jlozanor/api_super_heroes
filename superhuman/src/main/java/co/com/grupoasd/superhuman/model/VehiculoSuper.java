package co.com.grupoasd.superhuman.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Entity modelo VehiculoSuper.
 * 
 * @author Jean Khalo Lozano Ruiz
 * @version 2021/02/08
 */
@Entity
@Table(name = "sh_vehiculo_super")
public class VehiculoSuper extends AuditoriaEntity {

	/**
	 * Serial
	 */
	private static final long serialVersionUID = 1L;

	// Variables
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@NotNull
	@NotBlank
	@Length(max = 250)
	private String detalle;

	@JsonIgnore
	@NotNull
	@OneToOne
	private SuperHumano superHumano;

	@NotNull
	@OneToOne
	private Catalogo tipoVehiculo;

	/**
	 * Constructor.
	 */
	public VehiculoSuper() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructor con parametros.
	 * 
	 * @param detalle
	 * @param superHumano
	 * @param tipoVehiculo
	 */
	public VehiculoSuper(@NotNull @NotBlank @Length(max = 250) String detalle, @NotNull SuperHumano superHumano,
			@NotNull Catalogo tipoVehiculo) {
		super();
		this.detalle = detalle;
		this.superHumano = superHumano;
		this.tipoVehiculo = tipoVehiculo;
	}

	public String getDetalle() {
		return detalle;
	}

	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

	public SuperHumano getSuperHumano() {
		return superHumano;
	}

	public void setSuperHumano(SuperHumano superHumano) {
		this.superHumano = superHumano;
	}

	public Catalogo getTipoVehiculo() {
		return tipoVehiculo;
	}

	public void setTipoVehiculo(Catalogo tipoVehiculo) {
		this.tipoVehiculo = tipoVehiculo;
	}

}
