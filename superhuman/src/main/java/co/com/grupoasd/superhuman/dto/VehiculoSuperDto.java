package co.com.grupoasd.superhuman.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

/**
 * Clase DTO VehiculoSuperDto.
 * 
 * @author Jean Khalo Lozano Ruiz
 * @version 2021/02/08
 */
public class VehiculoSuperDto {
	
	// Variables
	@NotNull(message = "detalle no debe ser nulo")
	@NotBlank(message = "detalle no debe ser vacio")
	@Length(max = 250)
	private String detalle;
	
	@NotNull(message = "tipoVehiculo no debe ser nulo")
	@Valid
	private CatalogoDto tipoVehiculo;

	/**
	 * Constructor.
	 */
	public VehiculoSuperDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Constructor con parametros.
	 * @param detalle
	 * @param tipoVehiculo
	 */
	public VehiculoSuperDto(
			@NotNull(message = "detalle no debe ser nulo") @NotBlank(message = "detalle no debe ser vacio") @Length(max = 250) String detalle,
			@NotNull(message = "tipoVehiculo no debe ser nulo") @NotBlank(message = "tipoVehiculo no debe ser vacio") CatalogoDto tipoVehiculo) {
		super();
		this.detalle = detalle;
		this.tipoVehiculo = tipoVehiculo;
	}

	public String getDetalle() {
		return detalle;
	}

	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

	public CatalogoDto getTipoVehiculo() {
		return tipoVehiculo;
	}

	public void setTipoVehiculo(CatalogoDto tipoVehiculo) {
		this.tipoVehiculo = tipoVehiculo;
	}
	
}
