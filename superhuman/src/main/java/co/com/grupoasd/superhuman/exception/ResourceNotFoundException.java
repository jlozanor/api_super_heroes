package co.com.grupoasd.superhuman.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Clase para controlar las excepciones not found 404.
 * 
 * @author Jean Khalo Lozano Ruiz
 * @version 2021/02/08
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends Exception {

	// Serial
	private static final long serialVersionUID = 1L;

	/**
	 * Mensaje personalizado para la exceoción.
	 * 
	 * @param message
	 */
	public ResourceNotFoundException(String message) {
		super(message);
	}
}