package co.com.grupoasd.superhuman.service;

import org.springframework.data.domain.Pageable;

import co.com.grupoasd.superhuman.dto.VehiculoSuperListRsDto;
import co.com.grupoasd.superhuman.dto.VehiculoSuperPageRsDto;
import co.com.grupoasd.superhuman.exception.ResourceNotFoundException;

/**
 * Interface service VehiculoSuperService.
 * 
 * @author Jean Khalo Lozano Ruiz
 * @version 2021/02/08
 */
public interface VehiculoSuperService {

	/**
	 * Lista todos los vehiculos pero con control de paginación.
	 * 
	 * @param pageable Control de paginación.
	 * @return VehiculoSuperPageRsDto Vehiculos paginados.
	 */
	VehiculoSuperPageRsDto listAllVehicles(Pageable pageable);

	/**
	 * Lista todos los vehiculos de un super humano especifico.
	 * 
	 * @param superHumanoId Id del super humano
	 * @return VehiculoSuperListRsDto Lista de los vehiculos encontrados.
	 * @throws ResourceNotFoundException Excepción si no encuentra la super humano.
	 */
	VehiculoSuperListRsDto listVehiclesBySuperHuman(long superHumanoId) throws ResourceNotFoundException;

}
