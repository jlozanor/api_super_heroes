package co.com.grupoasd.superhuman.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.com.grupoasd.superhuman.model.Humano;

/**
 * Repositorio Jpa HumanoRepository.
 * 
 * @author Jean Khalo Lozano Ruiz
 * @version 2021/02/08
 */
@Repository
public interface HumanoRepository extends JpaRepository<Humano, Long> {

}
