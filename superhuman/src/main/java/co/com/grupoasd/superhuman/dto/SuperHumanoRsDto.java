package co.com.grupoasd.superhuman.dto;

import co.com.grupoasd.superhuman.model.SuperHumano;

/**
 * Clase DTO SuperHumanoRsDto.
 * 
 * @author Jean Khalo Lozano Ruiz
 * @version 2021/02/08
 */
public class SuperHumanoRsDto {

	// Variable
	private SuperHumano superHumano;

	/**
	 * Constructor
	 */
	public SuperHumanoRsDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructor con parametro.
	 * @param superHumano
	 */
	public SuperHumanoRsDto(SuperHumano superHumano) {
		super();
		this.superHumano = superHumano;
	}

	public SuperHumano getSuperHumano() {
		return superHumano;
	}

	public void setSuperHumano(SuperHumano superHumano) {
		this.superHumano = superHumano;
	}
	
}
