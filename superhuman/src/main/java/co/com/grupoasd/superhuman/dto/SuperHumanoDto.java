package co.com.grupoasd.superhuman.dto;

import java.time.LocalDate;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

/**
 * Clase DTO SuperHumanoDto.
 * 
 * @author Jean Khalo Lozano Ruiz
 * @version 2021/02/08
 */
public class SuperHumanoDto {

	// Variables
	@NotNull(message = "primerNombre no debe ser nulo")
	@NotBlank(message = "primerNombre no debe ser vacio")
	@Length(min = 2, max = 20)
	private String primerNombre;
	
	@Length(min = 2, max = 20)
	private String segundoNombre;
	
	@NotNull(message = "primerApellido no debe ser nulo")
	@NotBlank(message = "primerApellido no debe ser vacio")
	@Length(min = 2, max = 20)
	private String primerApellido;
	
	@Length(min = 2, max = 20)
	private String segundoApellido;
	
	@NotNull(message = "genero no debe ser nulo")
	@Valid
	private CatalogoDto genero;
	
	@NotNull(message = "fechaNacimiento no debe ser nulo")
	private LocalDate fechaNacimiento;
	
	@NotNull(message = "alterEgo no debe ser nulo")
	@NotBlank(message = "alterEgo no debe ser vacio")
	@Length(max = 20)
	private String alterEgo;
	
	@NotNull(message = "isHeroe no debe ser nulo")
	private Boolean isHeroe;
	
	@NotNull(message = "lugarOperacionId no debe ser nulo")
	private long lugarOperacionId;
	
	@NotNull(message = "condicion no debe ser nulo")
	@Valid
	private CatalogoDto condicion;
	
	@NotNull(message = "detalle no debe ser nulo")
	@NotBlank(message = "detalle no debe ser vacio")
	@Length(max = 250)
	private String detalle;
	
	@NotNull(message = "poderes no debe ser nulo")
	@Valid
	private List<CatalogoDto> poderes;
	
	@Valid
	private List<VehiculoSuperDto> vehiculos;

	/**
	 * Constructor.
	 */
	public SuperHumanoDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Constructor con parametros.
	 * @param primerNombre
	 * @param segundoNombre
	 * @param primerApellido
	 * @param segundoApellido
	 * @param genero
	 * @param fechaNacimiento
	 * @param alterEgo
	 * @param isHeroe
	 * @param lugarOperacionId
	 * @param condicion
	 * @param detalle
	 * @param poderes
	 * @param vehiculos
	 */
	public SuperHumanoDto(
			@NotNull(message = "primerNombre no debe ser nulo") @NotBlank(message = "primerNombre no debe ser vacio") @Length(min = 2, max = 20) String primerNombre,
			@Length(min = 2, max = 20) String segundoNombre,
			@NotNull(message = "primerApellido no debe ser nulo") @NotBlank(message = "primerApellido no debe ser vacio") @Length(min = 2, max = 20) String primerApellido,
			@Length(min = 2, max = 20) String segundoApellido,
			@NotNull(message = "genero no debe ser nulo") @Valid CatalogoDto genero,
			@NotNull(message = "fechaNacimiento no debe ser nulo") LocalDate fechaNacimiento,
			@NotNull(message = "alterEgo no debe ser nulo") @NotBlank(message = "alterEgo no debe ser vacio") @Length(max = 20) String alterEgo,
			@NotNull(message = "isHeroe no debe ser nulo") Boolean isHeroe,
			@NotNull(message = "lugarOperacionId no debe ser nulo") long lugarOperacionId,
			@NotNull(message = "condicion no debe ser nulo") @Valid CatalogoDto condicion,
			@NotNull(message = "detalle no debe ser nulo") @NotBlank(message = "detalle no debe ser vacio") @Length(max = 250) String detalle,
			@NotNull(message = "poderes no debe ser nulo") @Valid List<CatalogoDto> poderes,
			@Valid List<VehiculoSuperDto> vehiculos) {
		super();
		this.primerNombre = primerNombre;
		this.segundoNombre = segundoNombre;
		this.primerApellido = primerApellido;
		this.segundoApellido = segundoApellido;
		this.genero = genero;
		this.fechaNacimiento = fechaNacimiento;
		this.alterEgo = alterEgo;
		this.isHeroe = isHeroe;
		this.lugarOperacionId = lugarOperacionId;
		this.condicion = condicion;
		this.detalle = detalle;
		this.poderes = poderes;
		this.vehiculos = vehiculos;
	}

	public String getPrimerNombre() {
		return primerNombre;
	}

	public void setPrimerNombre(String primerNombre) {
		this.primerNombre = primerNombre;
	}

	public String getSegundoNombre() {
		return segundoNombre;
	}

	public void setSegundoNombre(String segundoNombre) {
		this.segundoNombre = segundoNombre;
	}

	public String getPrimerApellido() {
		return primerApellido;
	}

	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}

	public String getSegundoApellido() {
		return segundoApellido;
	}

	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}

	public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(LocalDate fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getAlterEgo() {
		return alterEgo;
	}

	public void setAlterEgo(String alterEgo) {
		this.alterEgo = alterEgo;
	}
	
	public Boolean getIsHeroe() {
		return isHeroe;
	}

	public void setIsHeroe(Boolean isHeroe) {
		this.isHeroe = isHeroe;
	}

	public long getLugarOperacionId() {
		return lugarOperacionId;
	}

	public void setLugarOperacionId(long lugarOperacionId) {
		this.lugarOperacionId = lugarOperacionId;
	}

	public String getDetalle() {
		return detalle;
	}

	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

	public List<VehiculoSuperDto> getVehiculos() {
		return vehiculos;
	}

	public void setVehiculos(List<VehiculoSuperDto> vehiculos) {
		this.vehiculos = vehiculos;
	}

	public CatalogoDto getGenero() {
		return genero;
	}

	public void setGenero(CatalogoDto genero) {
		this.genero = genero;
	}

	public CatalogoDto getCondicion() {
		return condicion;
	}

	public void setCondicion(CatalogoDto condicion) {
		this.condicion = condicion;
	}

	public List<CatalogoDto> getPoderes() {
		return poderes;
	}

	public void setPoderes(List<CatalogoDto> poderes) {
		this.poderes = poderes;
	}
	
}
