package co.com.grupoasd.superhuman.dto;

import org.springframework.data.domain.Page;

import co.com.grupoasd.superhuman.model.VehiculoSuper;

/**
 * Clase DTO VehiculoSuperPageRsDto.
 * 
 * @author Jean Khalo Lozano Ruiz
 * @version 2021/02/08
 */
public class VehiculoSuperPageRsDto {

	// Variables
	private Page<VehiculoSuper> vehiculos;

	/**
	 * Constructor.
	 */
	public VehiculoSuperPageRsDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructor con parametros.
	 * @param vehiculos
	 */
	public VehiculoSuperPageRsDto(Page<VehiculoSuper> vehiculos) {
		super();
		this.vehiculos = vehiculos;
	}

	public Page<VehiculoSuper> getVehiculos() {
		return vehiculos;
	}

	public void setVehiculos(Page<VehiculoSuper> vehiculos) {
		this.vehiculos = vehiculos;
	}
	
}
