package co.com.grupoasd.superhuman.controller;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import co.com.grupoasd.superhuman.dto.SuperHumanoDto;
import co.com.grupoasd.superhuman.dto.SuperHumanoListRsDto;
import co.com.grupoasd.superhuman.dto.SuperHumanoPageRsDto;
import co.com.grupoasd.superhuman.dto.SuperHumanoRsDto;
import co.com.grupoasd.superhuman.exception.ResourceNotFoundException;
import co.com.grupoasd.superhuman.model.SuperHumano;
import co.com.grupoasd.superhuman.service.SuperHumanoService;
import co.com.grupoasd.superhuman.specification.builder.SuperHumanoSpecBuilder;
import io.swagger.annotations.ApiOperation;

/**
 * Clase controlador para la gestión de superhumanos
 * 
 * @author Jean Khalo Lozano Ruiz
 * @version 2021/02/08
 */
@ApiOperation(value = "Controlador Superhumanos", notes = "Controlador para la gestión de superhumanos")
@CrossOrigin(origins = "*")
@Controller
@RequestMapping("/superhuman")
public class SuperHumanoController {

	// Variables
	@Autowired
	private SuperHumanoService superHumanoService;

	/**
	 * Crea nuevo superhumano con identificación unica.
	 * 
	 * @param superHumanoDto Datos requeridos para la creación del super humano.
	 * @return SuperHumanoRsDto Superhumano creado.
	 * @throws ResourceNotFoundException Registro no encontrado.
	 */
	@ApiOperation(value = "Crear Superhumano", notes = "Crea nuevo superhumano con identificación unica")
	@PostMapping(value = "/create")
	public ResponseEntity<SuperHumanoRsDto> createSuperHuman(@Valid @RequestBody SuperHumanoDto superHumanoDto)
			throws ResourceNotFoundException {
		return ResponseEntity.ok(superHumanoService.createSuperHuman(superHumanoDto));
	}

	/**
	 * Listo todos los superhumanos almacenados en db, con control de paginación.
	 * 
	 * @param pageable Paginación deseada por el cliente.
	 * @return SuperHumanoPageRsDto Lista paginada de los superhumanos.
	 */
	@ApiOperation(value = "Listar todos los superhumano", notes = "Listo todos los superhumanos almacenados en db, con control de paginación")
	@GetMapping(value = "/list")
	public ResponseEntity<SuperHumanoPageRsDto> listAllSuperHuman(
			@PageableDefault(page = 0, size = 2) Pageable pageable) {
		return ResponseEntity.ok(superHumanoService.listAllSuperHuman(pageable));
	}

	/**
	 * Busca los superhumanos por los datos de la entidad SuperHumano y Por los
	 * atributos de Ciudad. Tipos de busqueda (: igual o like) (> mayor o igual) (<
	 * menor o igual) (_ buscar por parametros de la tabla ciudad). Ejemplo:
	 * /superhuman/search?search=alterEgo:prueba,nombre_Washin
	 * 
	 * @param search Ejemplo:
	 *               /superhuman/search?search=alterEgo:prueba,nombre_Washin.
	 * @return SuperHumanoListRsDto Lista de superhumanos que coinciden con los
	 *         filtros.
	 */
	@ApiOperation(value = "Filtra Superhumanos", notes = "Busca los superhumanos por los datos de la entidad SuperHumano y Por los atributos de Ciudad "
			+ "Tipos de busqueda (: igual o like) (> mayor o igual) (< menor o igual) "
			+ "(_ buscar por parametros de la tabla ciudad) "
			+ "Ejemplo: /superhuman/search?search=alterEgo:prueba,nombre_Washin ")
	@GetMapping(value = "/search")
	public ResponseEntity<SuperHumanoListRsDto> findSuperHumanByFilters(@RequestParam(value = "search") String search) {

		SuperHumanoSpecBuilder builder = new SuperHumanoSpecBuilder();
		Pattern pattern = Pattern.compile("(\\w+?)(:|<|>|_)(\\w+?),", Pattern.UNICODE_CHARACTER_CLASS);
		Matcher matcher = pattern.matcher(search + ",");
		while (matcher.find()) {
			builder.with(matcher.group(1), matcher.group(2), matcher.group(3));
		}

		Specification<SuperHumano> spec = builder.build();

		return ResponseEntity.ok(superHumanoService.listByFilters(spec));
	}

	/**
	 * Actualiza superhumano buscado por superHumanoid.
	 * 
	 * @param superHumanoId  Id del superHumano a editar.
	 * @param superHumanoDto Datos requeridos para la edición del super humano.
	 * @return SuperHumanoRsDto Superhumano editado.
	 * @throws ResourceNotFoundException Registro no encontrado.
	 */
	@ApiOperation(value = "Actualiza Superhumano", notes = "Actualiza superhumano buscado por superHumanoid")
	@PutMapping(value = "/update/{id}")
	public ResponseEntity<SuperHumanoRsDto> updateSuperHuman(@PathVariable(value = "id") Long superHumanoId,
			@Valid @RequestBody SuperHumanoDto superHumanoDto) throws ResourceNotFoundException {
		return ResponseEntity.ok(superHumanoService.updateSuperHuman(superHumanoId, superHumanoDto));
	}
}
