package co.com.grupoasd.superhuman.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.com.grupoasd.superhuman.model.TipoCatalogo;

/**
 * Repositorio Jpa TipoCatalogoRepository.
 * 
 * @author Jean Khalo Lozano Ruiz
 * @version 2021/02/08
 */
@Repository
public interface TipoCatalogoRepository extends JpaRepository<TipoCatalogo, Long> {

}
