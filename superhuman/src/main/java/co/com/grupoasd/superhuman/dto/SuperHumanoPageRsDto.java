package co.com.grupoasd.superhuman.dto;

import org.springframework.data.domain.Page;

import co.com.grupoasd.superhuman.model.SuperHumano;

/**
 * Clase DTO SuperHumanoPageRsDto.
 * 
 * @author Jean Khalo Lozano Ruiz
 * @version 2021/02/08
 */
public class SuperHumanoPageRsDto {

	// Variables
	private Page<SuperHumano> superHumanos;

	/**
	 * Constructor.
	 */
	public SuperHumanoPageRsDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructor con parametros.
	 * @param superHumanos
	 */
	public SuperHumanoPageRsDto(Page<SuperHumano> superHumanos) {
		super();
		this.superHumanos = superHumanos;
	}

	public Page<SuperHumano> getSuperHumanos() {
		return superHumanos;
	}

	public void setSuperHumanos(Page<SuperHumano> superHumanos) {
		this.superHumanos = superHumanos;
	}
	
}
