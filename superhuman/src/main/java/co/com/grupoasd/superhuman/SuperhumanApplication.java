package co.com.grupoasd.superhuman;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SuperhumanApplication {

	public static void main(String[] args) {
		SpringApplication.run(SuperhumanApplication.class, args);
	}

}
