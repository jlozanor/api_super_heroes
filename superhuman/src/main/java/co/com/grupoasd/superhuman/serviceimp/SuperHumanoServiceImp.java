package co.com.grupoasd.superhuman.serviceimp;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import co.com.grupoasd.superhuman.dto.CatalogoDto;
import co.com.grupoasd.superhuman.dto.SuperHumanoDto;
import co.com.grupoasd.superhuman.dto.SuperHumanoListRsDto;
import co.com.grupoasd.superhuman.dto.SuperHumanoPageRsDto;
import co.com.grupoasd.superhuman.dto.SuperHumanoRsDto;
import co.com.grupoasd.superhuman.dto.VehiculoSuperDto;
import co.com.grupoasd.superhuman.exception.ResourceNotFoundException;
import co.com.grupoasd.superhuman.model.Catalogo;
import co.com.grupoasd.superhuman.model.Ciudad;
import co.com.grupoasd.superhuman.model.Humano;
import co.com.grupoasd.superhuman.model.SuperHumano;
import co.com.grupoasd.superhuman.model.VehiculoSuper;
import co.com.grupoasd.superhuman.repository.CatalogoRepository;
import co.com.grupoasd.superhuman.repository.CiudadRepository;
import co.com.grupoasd.superhuman.repository.HumanoRepository;
import co.com.grupoasd.superhuman.repository.SuperHumanoRepository;
import co.com.grupoasd.superhuman.repository.VehiculoSuperRepository;
import co.com.grupoasd.superhuman.service.SuperHumanoService;

/**
 * Clase service implementación SuperHumanoServiceImp, para la logica de
 * negocio.
 * 
 * @author Jean Khalo Lozano Ruiz
 * @version 2021/02/08
 */
@Service
public class SuperHumanoServiceImp implements SuperHumanoService {

	// Variables
	@Autowired
	private CatalogoRepository catalogoRepository;

	@Autowired
	private HumanoRepository humanoRepository;

	@Autowired
	private SuperHumanoRepository superHumanoRepository;

	@Autowired
	private VehiculoSuperRepository vehiculoSuperRepository;

	@Autowired
	private CiudadRepository ciudadRepository;

	// Para la internacionalización
	@Autowired
	private MessageSource mensajes;

	// Locale para lenguaje por defecto.
	private final Locale locale = LocaleContextHolder.getLocale();
	// private final Locale locale = Locale.US;

	// Inicializa log
	Logger logger = LoggerFactory.getLogger(SuperHumanoServiceImp.class);

	/**
	 * Sobreescritura, para más detalles javadoc service.
	 */
	@Transactional
	@Override
	public SuperHumanoRsDto createSuperHuman(SuperHumanoDto superHumanoDto) throws ResourceNotFoundException {

		logger.info("Inicia crear superhumano");

		Catalogo genero = catalogoRepository
				.findByIdAndTipoCatalogoId(superHumanoDto.getGenero().getId(),
						superHumanoDto.getGenero().getTipoCatalogoId())
				.orElseThrow(
						() -> new ResourceNotFoundException(
								mensajes.getMessage("registro.no.encontrado.id.and.tipo",
										new String[] { "Genero", String.valueOf(superHumanoDto.getGenero().getId()),
												String.valueOf(superHumanoDto.getGenero().getTipoCatalogoId()) },
										locale)));

		Catalogo condicion = catalogoRepository
				.findByIdAndTipoCatalogoId(superHumanoDto.getCondicion().getId(),
						superHumanoDto.getCondicion().getTipoCatalogoId())
				.orElseThrow(
						() -> new ResourceNotFoundException(mensajes.getMessage("registro.no.encontrado.id.and.tipo",
								new String[] { "Condición", String.valueOf(superHumanoDto.getCondicion().getId()),
										String.valueOf(superHumanoDto.getCondicion().getTipoCatalogoId()) },
								locale)));

		Ciudad lugarOperacion = ciudadRepository.findById(superHumanoDto.getLugarOperacionId())
				.orElseThrow(() -> new ResourceNotFoundException(mensajes.getMessage("registro.no.encontrado",
						new String[] { "Lugar de operación", String.valueOf(superHumanoDto.getLugarOperacionId()) },
						locale)));

		List<Catalogo> poderes = new ArrayList<Catalogo>();

		for (CatalogoDto poder : superHumanoDto.getPoderes()) {
			poderes.add(
					catalogoRepository.findByIdAndTipoCatalogoId(poder.getId(), poder.getTipoCatalogoId())
							.orElseThrow(() -> new ResourceNotFoundException(mensajes.getMessage(
									"registro.no.encontrado.id.and.tipo", new String[] { "Poder",
											String.valueOf(poder.getId()), String.valueOf(poder.getTipoCatalogoId()) },
									locale))));
		}

		Humano human = new Humano();
		human.setFechaNacimiento(superHumanoDto.getFechaNacimiento());
		human.setPrimerApellido(superHumanoDto.getPrimerApellido());
		human.setPrimerNombre(superHumanoDto.getPrimerNombre());
		human.setSegundoApellido(superHumanoDto.getSegundoApellido());
		human.setSegundoNombre(superHumanoDto.getSegundoNombre());
		human.setGenero(genero);

		SuperHumano superH = new SuperHumano();
		superH.setAlterEgo(superHumanoDto.getAlterEgo());
		superH.setDetalle(superHumanoDto.getDetalle());
		superH.setHeroe(superHumanoDto.getIsHeroe());
		superH.setHumano(human);
		superH.setIdentificacion(generarCodigoUnicoSuperHumano());
		superH.setCondicion(condicion);
		superH.setLugarOperacion(lugarOperacion);
		superH.setPoderes(poderes);

		List<VehiculoSuper> vehiculosSuper = new ArrayList<VehiculoSuper>();

		// Valida que se hayan enviado datos de vehiculos del superhumano
		if (!superHumanoDto.getVehiculos().isEmpty()) {

			for (VehiculoSuperDto vehiculoDto : superHumanoDto.getVehiculos()) {

				VehiculoSuper vehiculo = new VehiculoSuper();

				vehiculo.setTipoVehiculo(catalogoRepository
						.findByIdAndTipoCatalogoId(vehiculoDto.getTipoVehiculo().getId(),
								vehiculoDto.getTipoVehiculo().getTipoCatalogoId())
						.orElseThrow(() -> new ResourceNotFoundException(mensajes.getMessage(
								"registro.no.encontrado.id.and.tipo",
								new String[] { "Tipo vehiculo", String.valueOf(vehiculoDto.getTipoVehiculo().getId()),
										String.valueOf(vehiculoDto.getTipoVehiculo().getTipoCatalogoId()) },
								locale))));

				vehiculo.setSuperHumano(superH);
				vehiculo.setDetalle(vehiculoDto.getDetalle());

				vehiculosSuper.add(vehiculo);

			}

		}

		// Guardar todo
		humanoRepository.save(human);
		superHumanoRepository.save(superH);
		// Guarda vehiculos si estos fueron enviados en la petición
		if (!vehiculosSuper.isEmpty()) {
			vehiculoSuperRepository.saveAll(vehiculosSuper);
		}

		logger.info("Finaliza creación del superhumano");

		return new SuperHumanoRsDto(superH);
	}

	/**
	 * Metodo para crear un codigo aleatorio unico para el super humano a registrar.
	 * 
	 * @return String codigo generado.
	 */
	private String generarCodigoUnicoSuperHumano() {

		int valorEntero;

		do {
			valorEntero = (int) Math.floor(Math.random() * (999999 - 100000 + 1) + 100000);
		} while (superHumanoRepository.findByIdentificacion(String.valueOf(valorEntero)).isPresent());

		logger.info("Se crea identificación única" + valorEntero);

		return String.valueOf(valorEntero);
	}

	/**
	 * Sobreescritura, para más detalles javadoc service.
	 */
	@Override
	public SuperHumanoPageRsDto listAllSuperHuman(Pageable pageable) {
		logger.info("Lista todos los superhumano");
		return new SuperHumanoPageRsDto(superHumanoRepository.findAll(pageable));
	}

	/**
	 * Sobreescritura, para más detalles javadoc service.
	 */
	@Override
	public SuperHumanoListRsDto listByFilters(Specification<SuperHumano> spec) {
		logger.info("Lista superhumanos con los filtros enviados");
		return new SuperHumanoListRsDto(superHumanoRepository.findAll(spec));
	}

	/**
	 * Sobreescritura, para más detalles javadoc service.
	 */
	@Transactional(propagation = Propagation.NEVER, readOnly = false)
	@Override
	public SuperHumanoRsDto updateSuperHuman(long superHumanoId, SuperHumanoDto superHumanoDto)
			throws ResourceNotFoundException {

		logger.info("Inicia actualizar superhumano con id :: " + superHumanoId);

		Catalogo genero = catalogoRepository
				.findByIdAndTipoCatalogoId(superHumanoDto.getGenero().getId(),
						superHumanoDto.getGenero().getTipoCatalogoId())
				.orElseThrow(
						() -> new ResourceNotFoundException(
								mensajes.getMessage("registro.no.encontrado.id.and.tipo",
										new String[] { "Genero", String.valueOf(superHumanoDto.getGenero().getId()),
												String.valueOf(superHumanoDto.getGenero().getTipoCatalogoId()) },
										locale)));

		Catalogo condicion = catalogoRepository
				.findByIdAndTipoCatalogoId(superHumanoDto.getCondicion().getId(),
						superHumanoDto.getCondicion().getTipoCatalogoId())
				.orElseThrow(
						() -> new ResourceNotFoundException(mensajes.getMessage("registro.no.encontrado.id.and.tipo",
								new String[] { "Condición", String.valueOf(superHumanoDto.getCondicion().getId()),
										String.valueOf(superHumanoDto.getCondicion().getTipoCatalogoId()) },
								locale)));

		Ciudad lugarOperacion = ciudadRepository.findById(superHumanoDto.getLugarOperacionId())
				.orElseThrow(() -> new ResourceNotFoundException(mensajes.getMessage("registro.no.encontrado",
						new String[] { "Lugar de operación", String.valueOf(superHumanoDto.getLugarOperacionId()) },
						locale)));

		List<Catalogo> poderes = new ArrayList<Catalogo>();

		for (CatalogoDto poder : superHumanoDto.getPoderes()) {
			poderes.add(
					catalogoRepository.findByIdAndTipoCatalogoId(poder.getId(), poder.getTipoCatalogoId())
							.orElseThrow(() -> new ResourceNotFoundException(mensajes.getMessage(
									"registro.no.encontrado.id.and.tipo", new String[] { "Poder",
											String.valueOf(poder.getId()), String.valueOf(poder.getTipoCatalogoId()) },
									locale))));
		}

		SuperHumano superH = superHumanoRepository.findById(superHumanoId)
				.orElseThrow(() -> new ResourceNotFoundException(mensajes.getMessage("registro.no.encontrado",
						new String[] { "Superhumano", String.valueOf(superHumanoId) }, locale)));

		Humano human = humanoRepository.findById(superH.getHumano().getId())
				.orElseThrow(() -> new ResourceNotFoundException(mensajes.getMessage("registro.no.encontrado",
						new String[] { "Humano", String.valueOf(superH.getHumano().getId()) }, locale)));
		human.setFechaNacimiento(superHumanoDto.getFechaNacimiento());
		human.setPrimerApellido(superHumanoDto.getPrimerApellido());
		human.setPrimerNombre(superHumanoDto.getPrimerNombre());
		human.setSegundoApellido(superHumanoDto.getSegundoApellido());
		human.setSegundoNombre(superHumanoDto.getSegundoNombre());
		human.setGenero(genero);

		superH.setAlterEgo(superHumanoDto.getAlterEgo());
		superH.setDetalle(superHumanoDto.getDetalle());
		superH.setHeroe(superHumanoDto.getIsHeroe());
		superH.setHumano(human);
		superH.setCondicion(condicion);
		superH.setLugarOperacion(lugarOperacion);
		superH.setPoderes(poderes);

		List<VehiculoSuper> vehiculosSuper = vehiculoSuperRepository.findBySuperHumano(superH);

		// Valida que se ingrese algun vehiculo o los elimina si no los tiene.
		if (superHumanoDto.getVehiculos() != null) {

			for (VehiculoSuperDto vehiculoDto : superHumanoDto.getVehiculos()) {

				VehiculoSuper vehiculo = new VehiculoSuper();

				vehiculo.setTipoVehiculo(catalogoRepository
						.findByIdAndTipoCatalogoId(vehiculoDto.getTipoVehiculo().getId(),
								vehiculoDto.getTipoVehiculo().getTipoCatalogoId())
						.orElseThrow(() -> new ResourceNotFoundException(mensajes.getMessage(
								"registro.no.encontrado.id.and.tipo",
								new String[] { "Tipo vehiculo", String.valueOf(vehiculoDto.getTipoVehiculo().getId()),
										String.valueOf(vehiculoDto.getTipoVehiculo().getTipoCatalogoId()) },
								locale))));

				vehiculo.setSuperHumano(superH);
				vehiculo.setDetalle(vehiculoDto.getDetalle());

				vehiculosSuper.add(vehiculo);

			}

		}

		// actualizar todo
		superHumanoRepository.save(superH);
		// Elimina o crea vehiculos dependiendo el caso
		if (superHumanoDto.getVehiculos() != null) {
			vehiculoSuperRepository.saveAll(vehiculosSuper);
		} else {
			vehiculoSuperRepository.deleteAll(vehiculosSuper);
		}

		logger.info("Finaliza actualizar superhumano con id :: " + superHumanoId);

		return new SuperHumanoRsDto(superH);
	}

}
