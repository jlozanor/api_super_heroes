package co.com.grupoasd.superhuman.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import co.com.grupoasd.superhuman.dto.VehiculoSuperListRsDto;
import co.com.grupoasd.superhuman.dto.VehiculoSuperPageRsDto;
import co.com.grupoasd.superhuman.exception.ResourceNotFoundException;
import co.com.grupoasd.superhuman.service.VehiculoSuperService;
import io.swagger.annotations.ApiOperation;

/**
 * Clase controlador para la gestión de vehiculos super.
 * 
 * @author Jean Khalo Lozano Ruiz
 * @version 2021/02/08
 */
@ApiOperation(value = "Controlador VehiculosSuper", notes = "Controlador para la gestión de vehiculos super")
@CrossOrigin(origins = "*")
@Controller
@RequestMapping("/vehiculo-super")
public class VehiculoSuperController {

	// Variables.
	@Autowired
	private VehiculoSuperService vehiculoSuperService;

	/**
	 * Lista todos los vehiculos pero con control de paginación.
	 * 
	 * @param pageable Control de paginación.
	 * @return VehiculoSuperPageRsDto Vehiculos paginados.
	 */
	@ApiOperation(value = "Listar vehiculos", notes = "Lista todos los vehiculos registrados")
	@GetMapping(value = "/list")
	public ResponseEntity<VehiculoSuperPageRsDto> listAllVehicles(
			@PageableDefault(page = 0, size = 2) Pageable pageable) {
		return ResponseEntity.ok(vehiculoSuperService.listAllVehicles(pageable));
	}

	/**
	 * Lista todos los vehiculos de un super humano especifico.
	 * 
	 * @param superHumanoId Id del super humano
	 * @return VehiculoSuperListRsDto Lista de los vehiculos encontrados.
	 * @throws ResourceNotFoundException Excepción si no encuentra la super humano.
	 */
	@ApiOperation(value = "Listar vehiculos de super humano", notes = "Lista todos los vehiculos registrados a x superhumano, se requiere ID de superHumanoId")
	@GetMapping(value = "/superhumano/{id}")
	public ResponseEntity<VehiculoSuperListRsDto> listVehiclesBySuperHuman(
			@PathVariable(value = "id") Long superHumanoId) throws ResourceNotFoundException {
		return ResponseEntity.ok(vehiculoSuperService.listVehiclesBySuperHuman(superHumanoId));
	}

}
