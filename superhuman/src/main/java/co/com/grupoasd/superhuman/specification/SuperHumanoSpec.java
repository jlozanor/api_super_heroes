package co.com.grupoasd.superhuman.specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import co.com.grupoasd.superhuman.dto.SearchCriteriaDto;
import co.com.grupoasd.superhuman.model.Ciudad;
import co.com.grupoasd.superhuman.model.SuperHumano;

/**
 * Especificación para filtrar busqueda jpa.
 * 
 * @author Jean Khalo Lozano Ruiz
 * @version 2021/02/08
 */
public class SuperHumanoSpec implements Specification<SuperHumano> {

	/**
	 * Serial
	 */
	private static final long serialVersionUID = 1L;

	// Variables
	private SearchCriteriaDto criteria;

	/**
	 * Contructor con parametros.
	 * @param criteria
	 */
	public SuperHumanoSpec(SearchCriteriaDto criteria) {
		super();
		this.criteria = criteria;
	}

	/**
	 * Sobreescritura, e implementación para definir las condiciones para los filtros.
	 */
	@Override
	public Predicate toPredicate(Root<SuperHumano> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {

		if (criteria.getOperation().equalsIgnoreCase("_")) {
			Join<SuperHumano, Ciudad> tags = root.join("lugarOperacion", JoinType.INNER);
			return criteriaBuilder.like(tags.get(criteria.getKey()), "%" + criteria.getValue().toString() + "%");
		} else if (criteria.getOperation().equalsIgnoreCase(">")) {
			return criteriaBuilder.greaterThanOrEqualTo(root.<String>get(criteria.getKey()),
					criteria.getValue().toString());
		} else if (criteria.getOperation().equalsIgnoreCase("<")) {
			return criteriaBuilder.lessThanOrEqualTo(root.<String>get(criteria.getKey()),
					criteria.getValue().toString());
		} else if (criteria.getOperation().equalsIgnoreCase(":")) {
			if (root.get(criteria.getKey()).getJavaType() == String.class) {
				return criteriaBuilder.like(root.<String>get(criteria.getKey()), "%" + criteria.getValue() + "%");
			} else {
				return criteriaBuilder.equal(root.get(criteria.getKey()), criteria.getValue());
			}
		}

		return null;
	}
}
