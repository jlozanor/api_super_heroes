package co.com.grupoasd.superhuman.specification.builder;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.jpa.domain.Specification;

import co.com.grupoasd.superhuman.dto.SearchCriteriaDto;
import co.com.grupoasd.superhuman.model.SuperHumano;
import co.com.grupoasd.superhuman.specification.SuperHumanoSpec;

/**
 * Especificación Builder para filtrar busqueda jpa.
 * 
 * @author Jean Khalo Lozano Ruiz
 * @version 2021/02/08
 */
public class SuperHumanoSpecBuilder {

	// Variables
	private final List<SearchCriteriaDto> params;

	/**
	 * Constructor.
	 */
	public SuperHumanoSpecBuilder() {
		params = new ArrayList<SearchCriteriaDto>();
	}

	/**
	 * Constructor con parametros.
	 * 
	 * @param key
	 * @param operation
	 * @param value
	 * @return SuperHumanoSpecBuilder
	 */
	public SuperHumanoSpecBuilder with(String key, String operation, Object value) {
		params.add(new SearchCriteriaDto(key, operation, value));
		return this;
	}

	/**
	 * Concatena los diferentes filtros.
	 * 
	 * @return Specification<SuperHumano>
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Specification<SuperHumano> build() {
		if (params.size() == 0) {
			return null;
		}

		List<Specification> specs = params.stream().map(SuperHumanoSpec::new).collect(Collectors.toList());

		Specification result = specs.get(0);

		for (int i = 1; i < params.size(); i++) {
			/**
			 * result = params.get(i) != null ? Specification.where(result).or(specs.get(i))
			 * : Specification.where(result).and(specs.get(i));
			 */
			result = Specification.where(result).and(specs.get(i));
		}
		return result;
	}

}
