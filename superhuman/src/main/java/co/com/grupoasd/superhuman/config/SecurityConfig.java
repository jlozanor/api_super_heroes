package co.com.grupoasd.superhuman.config;

import org.keycloak.adapters.springboot.KeycloakSpringBootConfigResolver;
import org.keycloak.adapters.springsecurity.KeycloakSecurityComponents;
import org.keycloak.adapters.springsecurity.authentication.KeycloakAuthenticationProvider;
import org.keycloak.adapters.springsecurity.config.KeycloakWebSecurityConfigurerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.authority.mapping.SimpleAuthorityMapper;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.web.authentication.session.RegisterSessionAuthenticationStrategy;
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;

/**
 * Esta clase contiene la configuración de seguridad de la api y define las rutas por roles.
 * @author Jean Khalo Lozano Ruiz
 * @version 2021/02/08
 */
@Configuration
@EnableWebSecurity
@ComponentScan(basePackageClasses = KeycloakSecurityComponents.class)
public class SecurityConfig extends KeycloakWebSecurityConfigurerAdapter {

	/**
	 * Configuración global de la seguridad.
	 * @param auth Datos de la autorización.
	 * @throws Exception
	 */
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		KeycloakAuthenticationProvider keycloakAuthenticationProvider = keycloakAuthenticationProvider();
		keycloakAuthenticationProvider.setGrantedAuthoritiesMapper(new SimpleAuthorityMapper());
		auth.authenticationProvider(keycloakAuthenticationProvider);
	}

	/**
	 * Gestiona la seguridad por medio de la dependencia KeyCloak.
	 * @return KeycloakSpringBootConfigResolver retorna la validación por keyCloak.
	 */
	@Bean
	public KeycloakSpringBootConfigResolver KeycloakConfigResolver() {
		return new KeycloakSpringBootConfigResolver();
	}

	/**
	 * Sobrescritura de sessionAuthenticationStrategy. Mas detalles en la implementación.
	 */
	@Bean
	@Override
	protected SessionAuthenticationStrategy sessionAuthenticationStrategy() {
		return new RegisterSessionAuthenticationStrategy(new SessionRegistryImpl());
	}

	/**
	 * Sobrescritura de configure. Mas detalles en la implementación.
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		super.configure(http);
		
		// Disable CSRF (cross site request forgery)
        http.csrf().disable();
        
        // No session will be created or used by spring security
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		
        // Entry points
		http.authorizeRequests()
		.antMatchers("/superhuman/create").hasRole("user")
		.antMatchers("/superhuman/search*").hasRole("user")
		.antMatchers("/superhuman/update/{id}").hasRole("user")
		.antMatchers("/vehiculo-super/superhumano/{id}").hasRole("user")
		.anyRequest()
		.permitAll();
	}

}
