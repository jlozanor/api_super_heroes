package co.com.grupoasd.superhuman.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.com.grupoasd.superhuman.model.SuperHumano;
import co.com.grupoasd.superhuman.model.VehiculoSuper;

/**
 * Repositorio Jpa VehiculoSuperRepository.
 * 
 * @author Jean Khalo Lozano Ruiz
 * @version 2021/02/08
 */
@Repository
public interface VehiculoSuperRepository extends JpaRepository<VehiculoSuper, Long> {

	/**
	 * Buscar vehiculos por superhumano.
	 * 
	 * @param superHumano
	 * @return List<VehiculoSuper>
	 */
	List<VehiculoSuper> findBySuperHumano(SuperHumano superHumano);

}
