package co.com.grupoasd.superhuman.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.MappedSuperclass;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

/**
 * Entity generica de auditoria.
 * 
 * @author Jean Khalo Lozano Ruiz
 * @version 2021/02/08
 */
@MappedSuperclass
public class AuditoriaEntity implements Serializable {

	/**
	 * Serial
	 */
	private static final long serialVersionUID = 1L;

	// Variables
	@NotNull
	private LocalDateTime fecha_creacion;

	@Version
	@NotNull
	private LocalDateTime fecha_actualizacion;

	/**
	 * Constructor.
	 */
	public AuditoriaEntity() {
		super();
		this.fecha_creacion = LocalDateTime.now();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructor con parametros.
	 * 
	 * @param fecha_creacion
	 * @param fecha_actualizacion
	 */
	public AuditoriaEntity(LocalDateTime fecha_creacion, LocalDateTime fecha_actualizacion) {
		super();
		this.fecha_creacion = fecha_creacion;
		this.fecha_actualizacion = fecha_actualizacion;
	}

	public LocalDateTime getFecha_creacion() {
		return fecha_creacion;
	}

	public void setFecha_creacion(LocalDateTime fecha_creacion) {
		this.fecha_creacion = fecha_creacion;
	}

	public LocalDateTime getFecha_actualizacion() {
		return fecha_actualizacion;
	}

	public void setFecha_actualizacion(LocalDateTime fecha_actualizacion) {
		this.fecha_actualizacion = fecha_actualizacion;
	}

}
