package co.com.grupoasd.superhuman.dto;

/**
 * Clase DTO SearchCriteriaDto especifica para filtrar con JpaSpecification.
 * 
 * @author Jean Khalo Lozano Ruiz
 * @version 2021/02/08
 */
public class SearchCriteriaDto {
	
	// Variables
	private String key;
    private String operation;
    private Object value;
    
    /**
     * Contructor.
     */
	public SearchCriteriaDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Contructor con parametros.
	 * @param key
	 * @param operation
	 * @param value
	 */
	public SearchCriteriaDto(String key, String operation, Object value) {
		super();
		this.key = key;
		this.operation = operation;
		this.value = value;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}
    
}
