package co.com.grupoasd.superhuman.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Clase DTO CatalogoDto.
 * 
 * @author Jean Khalo Lozano Ruiz
 * @version 2021/02/08
 */
public class CatalogoDto {

	// Variables
	@NotNull(message = "id no debe ser nulo")
	private long id;
	
	@NotNull(message = "tipoCatalogoId no debe ser nulo")
	private long tipoCatalogoId;

	/**
	 * Constructor.
	 */
	public CatalogoDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Constructor con parametros.
	 * @param id
	 * @param tipoCatalogoId
	 */
	public CatalogoDto(@NotNull(message = "id no debe ser nulo") @NotBlank(message = "id no debe ser vacio") long id,
			@NotNull(message = "tipoCatalogoId no debe ser nulo") @NotBlank(message = "tipoCatalogoId no debe ser vacio") long tipoCatalogoId) {
		super();
		this.id = id;
		this.tipoCatalogoId = tipoCatalogoId;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getTipoCatalogoId() {
		return tipoCatalogoId;
	}

	public void setTipoCatalogoId(long tipoCatalogoId) {
		this.tipoCatalogoId = tipoCatalogoId;
	}
	
}
