package co.com.grupoasd.superhuman.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

/**
 * Entity modelo Pais.
 * 
 * @author Jean Khalo Lozano Ruiz
 * @version 2021/02/08
 */
@Entity
@Table(name = "sh_pais")
public class Pais {

	// Variables
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@NotNull
	@NotBlank
	@Length(min = 1, max = 5)
	private String abreviatura;

	@NotNull
	@NotBlank
	@Length(min = 3, max = 20)
	private String nombre;

	/**
	 * Constructor.
	 */
	public Pais() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructor con parametros.
	 * 
	 * @param abreviatura
	 * @param nombre
	 */
	public Pais(@NotNull @NotBlank @Length(min = 1, max = 5) String abreviatura,
			@NotNull @NotBlank @Length(min = 3, max = 20) String nombre) {
		super();
		this.abreviatura = abreviatura;
		this.nombre = nombre;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getAbreviatura() {
		return abreviatura;
	}

	public void setAbreviatura(String abreviatura) {
		this.abreviatura = abreviatura;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}
