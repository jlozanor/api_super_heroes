package co.com.grupoasd.superhuman.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.com.grupoasd.superhuman.model.Catalogo;

/**
 * Repositorio Jpa CatalogoRepository.
 * 
 * @author Jean Khalo Lozano Ruiz
 * @version 2021/02/08
 */
@Repository
public interface CatalogoRepository extends JpaRepository<Catalogo, Long> {

	/**
	 * Metodo para buscar por id y TipoCatalogoId.
	 * 
	 * @param id
	 * @param tipoCatalogoId
	 * @return Optional<Catalogo>
	 */
	Optional<Catalogo> findByIdAndTipoCatalogoId(long id, long tipoCatalogoId);

}
