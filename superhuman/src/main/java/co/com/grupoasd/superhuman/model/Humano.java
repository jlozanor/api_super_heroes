package co.com.grupoasd.superhuman.model;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

/**
 * Entity modelo Humano.
 * 
 * @author Jean Khalo Lozano Ruiz
 * @version 2021/02/08
 */
@Entity
@Table(name = "sh_humano")
public class Humano extends AuditoriaEntity {

	/**
	 * Serial
	 */
	private static final long serialVersionUID = 1L;

	// Variables
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@NotNull
	@NotBlank
	@Length(min = 2, max = 20)
	private String primerNombre;

	@Length(min = 2, max = 20)
	private String segundoNombre;

	@NotNull
	@NotBlank
	@Length(min = 2, max = 20)
	private String primerApellido;

	@Length(min = 2, max = 20)
	private String segundoApellido;

	@NotNull
	@OneToOne
	private Catalogo genero;

	@NotNull
	private LocalDate fechaNacimiento;

	/**
	 * Constructor.
	 */
	public Humano() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructor con parametros.
	 * 
	 * @param primerNombre
	 * @param segundoNombre
	 * @param primerApellido
	 * @param segundoApellido
	 * @param genero
	 * @param fechaNacimiento
	 */
	public Humano(@NotNull @NotBlank @Length(min = 2, max = 20) String primerNombre,
			@Length(min = 2, max = 20) String segundoNombre,
			@NotNull @NotBlank @Length(min = 2, max = 20) String primerApellido,
			@Length(min = 2, max = 20) String segundoApellido, @NotNull Catalogo genero,
			@NotNull LocalDate fechaNacimiento) {
		super();
		this.primerNombre = primerNombre;
		this.segundoNombre = segundoNombre;
		this.primerApellido = primerApellido;
		this.segundoApellido = segundoApellido;
		this.genero = genero;
		this.fechaNacimiento = fechaNacimiento;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPrimerNombre() {
		return primerNombre;
	}

	public void setPrimerNombre(String primerNombre) {
		this.primerNombre = primerNombre;
	}

	public String getSegundoNombre() {
		return segundoNombre;
	}

	public void setSegundoNombre(String segundoNombre) {
		this.segundoNombre = segundoNombre;
	}

	public String getPrimerApellido() {
		return primerApellido;
	}

	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}

	public String getSegundoApellido() {
		return segundoApellido;
	}

	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}

	public Catalogo getGenero() {
		return genero;
	}

	public void setGenero(Catalogo genero) {
		this.genero = genero;
	}

	public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(LocalDate fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

}
