package co.com.grupoasd.superhuman.service;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import co.com.grupoasd.superhuman.dto.SuperHumanoDto;
import co.com.grupoasd.superhuman.dto.SuperHumanoListRsDto;
import co.com.grupoasd.superhuman.dto.SuperHumanoPageRsDto;
import co.com.grupoasd.superhuman.dto.SuperHumanoRsDto;
import co.com.grupoasd.superhuman.exception.ResourceNotFoundException;
import co.com.grupoasd.superhuman.model.SuperHumano;

/**
 * Interface service SuperHumanoService.
 * 
 * @author Jean Khalo Lozano Ruiz
 * @version 2021/02/08
 */
public interface SuperHumanoService {

	/**
	 * Crea nuevo superhumano con identificación unica.
	 * 
	 * @param superHumanoDto Datos requeridos para la creación del super humano.
	 * @return SuperHumanoRsDto Superhumano creado.
	 * @throws ResourceNotFoundException Registro no encontrado.
	 */
	SuperHumanoRsDto createSuperHuman(SuperHumanoDto superHumanoDto) throws ResourceNotFoundException;

	/**
	 * Listo todos los superhumanos almacenados en db, con control de paginación.
	 * 
	 * @param pageable Paginación deseada por el cliente.
	 * @return SuperHumanoPageRsDto Lista paginada de los superhumanos.
	 */
	SuperHumanoPageRsDto listAllSuperHuman(Pageable pageable);

	/**
	 * Busca los superhumanos por los datos de la entidad SuperHumano y Por los
	 * atributos de Ciudad. Tipos de busqueda (: igual o like) (> mayor o igual) (<
	 * menor o igual) (_ buscar por parametros de la tabla ciudad). Ejemplo:
	 * /superhuman/search?search=alterEgo:prueba,nombre_Washin
	 * 
	 * @param search Ejemplo:
	 *               /superhuman/search?search=alterEgo:prueba,nombre_Washin.
	 * @return SuperHumanoListRsDto Lista de superhumanos que coinciden con los
	 *         filtros.
	 */
	SuperHumanoListRsDto listByFilters(Specification<SuperHumano> spec);

	/**
	 * Actualiza superhumano buscado por superHumanoid.
	 * 
	 * @param superHumanoId  Id del superHumano a editar.
	 * @param superHumanoDto Datos requeridos para la edición del super humano.
	 * @return SuperHumanoRsDto Superhumano editado.
	 * @throws ResourceNotFoundException Registro no encontrado.
	 */
	SuperHumanoRsDto updateSuperHuman(long superHumanoId, SuperHumanoDto superHumanoDto)
			throws ResourceNotFoundException;

}
