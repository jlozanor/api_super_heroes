package co.com.grupoasd.superhuman.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.com.grupoasd.superhuman.model.Pais;

/**
 * Repositorio Jpa PaisRepository.
 * 
 * @author Jean Khalo Lozano Ruiz
 * @version 2021/02/08
 */
@Repository
public interface PaisRepository extends JpaRepository<Pais, Long> {

}
