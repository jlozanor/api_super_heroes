package co.com.grupoasd.superhuman.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import co.com.grupoasd.superhuman.model.SuperHumano;

/**
 * Repositorio Jpa SuperHumanoRepository.
 * 
 * @author Jean Khalo Lozano Ruiz
 * @version 2021/02/08
 */
@Repository
public interface SuperHumanoRepository extends JpaRepository<SuperHumano, Long>, JpaSpecificationExecutor<SuperHumano> {

	/**
	 * Metodo para buscar por identificación unica del super humano.
	 * 
	 * @param identificacion
	 * @return Optional<SuperHumano>
	 */
	Optional<SuperHumano> findByIdentificacion(String identificacion);

}
