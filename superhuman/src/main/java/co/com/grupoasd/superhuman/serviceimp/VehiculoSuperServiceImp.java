package co.com.grupoasd.superhuman.serviceimp;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import co.com.grupoasd.superhuman.dto.VehiculoSuperListRsDto;
import co.com.grupoasd.superhuman.dto.VehiculoSuperPageRsDto;
import co.com.grupoasd.superhuman.exception.ResourceNotFoundException;
import co.com.grupoasd.superhuman.model.SuperHumano;
import co.com.grupoasd.superhuman.repository.SuperHumanoRepository;
import co.com.grupoasd.superhuman.repository.VehiculoSuperRepository;
import co.com.grupoasd.superhuman.service.VehiculoSuperService;

/**
 * Clase service implementación VehiculoSuperServiceImp, para la logica de
 * negocio.
 * 
 * @author Jean Khalo Lozano Ruiz
 * @version 2021/02/08
 */
@Service
public class VehiculoSuperServiceImp implements VehiculoSuperService {

	// Variables
	@Autowired
	private VehiculoSuperRepository vehiculoSuperRepository;

	@Autowired
	private SuperHumanoRepository superHumanoRepository;

	// Para la internacionalización
	@Autowired
	private MessageSource mensajes;

	// Locale para lenguaje por defecto.
	private final Locale locale = LocaleContextHolder.getLocale();
	// private final Locale locale = Locale.US;

	// Inicializa log
	Logger logger = LoggerFactory.getLogger(VehiculoSuperServiceImp.class);

	/**
	 * Sobreescritura, para más detalles javadoc service.
	 */
	@Override
	public VehiculoSuperPageRsDto listAllVehicles(Pageable pageable) {
		logger.info("Lista todos los vehiculos");
		return new VehiculoSuperPageRsDto(vehiculoSuperRepository.findAll(pageable));
	}

	/**
	 * Sobreescritura, para más detalles javadoc service.
	 */
	@Override
	public VehiculoSuperListRsDto listVehiclesBySuperHuman(long superHumanoId) throws ResourceNotFoundException {

		logger.info("Lista todos los vehiculos del superhumano con id :: " + superHumanoId);

		SuperHumano superHumano = superHumanoRepository.findById(superHumanoId)
				.orElseThrow(() -> new ResourceNotFoundException(mensajes.getMessage("registro.no.encontrado",
						new String[] { "SuperHumano", String.valueOf(superHumanoId) }, locale)));

		return new VehiculoSuperListRsDto(vehiculoSuperRepository.findBySuperHumano(superHumano));
	}

}
