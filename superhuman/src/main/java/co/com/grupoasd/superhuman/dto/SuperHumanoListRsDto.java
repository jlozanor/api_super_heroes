package co.com.grupoasd.superhuman.dto;

import java.util.List;

import co.com.grupoasd.superhuman.model.SuperHumano;

/**
 * Clase DTO SuperHumanoListRsDto.
 * 
 * @author Jean Khalo Lozano Ruiz
 * @version 2021/02/08
 */
public class SuperHumanoListRsDto {

	// Variables
	private List<SuperHumano> superHumanos;

	/**
	 * Constructor.
	 */
	public SuperHumanoListRsDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Constructor con parametros.
	 * @param superHumanos
	 */
	public SuperHumanoListRsDto(List<SuperHumano> superHumanos) {
		super();
		this.superHumanos = superHumanos;
	}

	public List<SuperHumano> getSuperHumanos() {
		return superHumanos;
	}

	public void setSuperHumanos(List<SuperHumano> superHumanos) {
		this.superHumanos = superHumanos;
	}
	
}
