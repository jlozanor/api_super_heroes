package co.com.grupoasd.superhuman.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

/**
 * Entity modelo TipoCatalogo.
 * 
 * @author Jean Khalo Lozano Ruiz
 * @version 2021/02/08
 */
@Entity
@Table(name = "sh_tipo_catalogo")
public class TipoCatalogo {
	
	// Variables
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@NotNull
	@NotBlank
	@Length(min = 1, max = 10)
	private String codigo;
	
	@NotNull
	@NotBlank
	@Length(min = 2, max = 20)
	private String nombre;

	/**
	 * Constructor.
	 */
	public TipoCatalogo() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Constructor con parametros.
	 * @param codigo
	 * @param nombre
	 */
	public TipoCatalogo(@NotNull @NotBlank @Length(min = 1, max = 10) String codigo,
			@NotNull @NotBlank @Length(min = 2, max = 20) String nombre) {
		super();
		this.codigo = codigo;
		this.nombre = nombre;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}
