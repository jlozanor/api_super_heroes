package co.com.grupoasd.superhuman.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

/**
 * Entity modelo Ciudad.
 * 
 * @author Jean Khalo Lozano Ruiz
 * @version 2021/02/08
 */
@Entity
@Table(name = "sh_ciudad")
public class Ciudad {

	// Variables
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@NotNull
	@NotBlank
	@Length(min = 1, max = 5)
	private String abreviatura;

	@NotNull
	@NotBlank
	@Length(min = 3, max = 20)
	private String nombre;

	@NotNull
	@OneToOne
	private Pais pais;

	/**
	 * Constructor.
	 */
	public Ciudad() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructor con parametros.
	 * 
	 * @param abreviatura
	 * @param nombre
	 * @param pais
	 */
	public Ciudad(@NotNull @NotBlank @Length(min = 1, max = 5) String abreviatura,
			@NotNull @NotBlank @Length(min = 3, max = 20) String nombre, @NotNull Pais pais) {
		super();
		this.abreviatura = abreviatura;
		this.nombre = nombre;
		this.pais = pais;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getAbreviatura() {
		return abreviatura;
	}

	public void setAbreviatura(String abreviatura) {
		this.abreviatura = abreviatura;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Pais getPais() {
		return pais;
	}

	public void setPais(Pais pais) {
		this.pais = pais;
	}

}
