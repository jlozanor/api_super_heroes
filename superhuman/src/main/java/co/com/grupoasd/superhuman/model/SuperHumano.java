package co.com.grupoasd.superhuman.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

/**
 * Entity modelo SuperHumano.
 * 
 * @author Jean Khalo Lozano Ruiz
 * @version 2021/02/08
 */
@Entity
@Table(name = "sh_super_humano")
public class SuperHumano extends AuditoriaEntity {

	/**
	 * Serial
	 */
	private static final long serialVersionUID = 1L;

	// Variables
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@NotNull
	@NotBlank
	@Length(min = 5, max = 100)
	private String identificacion;

	@NotNull
	@NotBlank
	@Length(max = 20)
	private String alterEgo;

	@NotNull
	private boolean isHeroe;

	@NotNull
	@OneToOne
	private Ciudad lugarOperacion;

	@NotNull
	@OneToOne
	private Catalogo condicion;

	@NotNull
	@OneToOne
	private Humano humano;

	@NotNull
	@NotBlank
	@Length(max = 250)
	private String detalle;

	@ManyToMany
	@JoinTable(name = "sh_superhuman_x_poderes")
	private List<Catalogo> poderes;

	/**
	 * Constructor.
	 */
	public SuperHumano() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructor con parametros.
	 * 
	 * @param identificacion
	 * @param alterEgo
	 * @param isHeroe
	 * @param lugarOperacion
	 * @param condicion
	 * @param humano
	 * @param detalle
	 * @param poderes
	 */
	public SuperHumano(@NotNull @NotBlank @Length(min = 10, max = 100) String identificacion,
			@NotNull @NotBlank @Length(max = 20) String alterEgo, @NotNull boolean isHeroe,
			@NotNull Ciudad lugarOperacion, @NotNull Catalogo condicion, @NotNull Humano humano,
			@NotNull @NotBlank @Length(max = 250) String detalle, List<Catalogo> poderes) {
		super();
		this.identificacion = identificacion;
		this.alterEgo = alterEgo;
		this.isHeroe = isHeroe;
		this.lugarOperacion = lugarOperacion;
		this.condicion = condicion;
		this.humano = humano;
		this.detalle = detalle;
		this.poderes = poderes;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getIdentificacion() {
		return identificacion;
	}

	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}

	public String getAlterEgo() {
		return alterEgo;
	}

	public void setAlterEgo(String alterEgo) {
		this.alterEgo = alterEgo;
	}

	public boolean isHeroe() {
		return isHeroe;
	}

	public void setHeroe(boolean isHeroe) {
		this.isHeroe = isHeroe;
	}

	public Ciudad getLugarOperacion() {
		return lugarOperacion;
	}

	public void setLugarOperacion(Ciudad lugarOperacion) {
		this.lugarOperacion = lugarOperacion;
	}

	public Catalogo getCondicion() {
		return condicion;
	}

	public void setCondicion(Catalogo condicion) {
		this.condicion = condicion;
	}

	public Humano getHumano() {
		return humano;
	}

	public void setHumano(Humano humano) {
		this.humano = humano;
	}

	public String getDetalle() {
		return detalle;
	}

	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

	public List<Catalogo> getPoderes() {
		return poderes;
	}

	public void setPoderes(List<Catalogo> poderes) {
		this.poderes = poderes;
	}

}
