package co.com.grupoasd.superhuman.dto;

import java.util.List;

import co.com.grupoasd.superhuman.model.VehiculoSuper;

/**
 * Clase DTO VehiculoSuperListRsDto.
 * 
 * @author Jean Khalo Lozano Ruiz
 * @version 2021/02/08
 */
public class VehiculoSuperListRsDto {

	// Variable
	private List<VehiculoSuper> vehiculos;

	/**
	 * Constructor.
	 */
	public VehiculoSuperListRsDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructor con parametros.
	 * @param vehiculos
	 */
	public VehiculoSuperListRsDto(List<VehiculoSuper> vehiculos) {
		super();
		this.vehiculos = vehiculos;
	}

	public List<VehiculoSuper> getVehiculos() {
		return vehiculos;
	}

	public void setVehiculos(List<VehiculoSuper> vehiculos) {
		this.vehiculos = vehiculos;
	}
	
}
