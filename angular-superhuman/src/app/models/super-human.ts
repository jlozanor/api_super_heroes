declare module SuperHumanoPages {

  export interface Pais {
      id: number;
      abreviatura: string;
      nombre: string;
  }

  export interface LugarOperacion {
      id: number;
      abreviatura: string;
      nombre: string;
      pais: Pais;
  }

  export interface TipoCatalogo {
      id: number;
      codigo: string;
      nombre: string;
  }

  export interface Condicion {
      id: number;
      codigo: string;
      nombre: string;
      descripcion: string;
      tipoCatalogo: TipoCatalogo;
  }

  export interface TipoCatalogo2 {
      id: number;
      codigo: string;
      nombre: string;
  }

  export interface Genero {
      id: number;
      codigo: string;
      nombre: string;
      descripcion: string;
      tipoCatalogo: TipoCatalogo2;
  }

  export interface Humano {
      fecha_creacion: Date;
      fecha_actualizacion: Date;
      id: number;
      primerNombre: string;
      segundoNombre: string;
      primerApellido: string;
      segundoApellido: string;
      genero: Genero;
      fechaNacimiento: string;
  }

  export interface TipoCatalogo3 {
      id: number;
      codigo: string;
      nombre: string;
  }

  export interface Podere {
      id: number;
      codigo: string;
      nombre: string;
      descripcion: string;
      tipoCatalogo: TipoCatalogo3;
  }

  export interface Content {
      fecha_creacion: Date;
      fecha_actualizacion: Date;
      id: number;
      identificacion: string;
      alterEgo: string;
      lugarOperacion: LugarOperacion;
      condicion: Condicion;
      humano: Humano;
      detalle: string;
      poderes: Podere[];
      heroe: boolean;
  }

  export interface Sort {
      sorted: boolean;
      unsorted: boolean;
      empty: boolean;
  }

  export interface Pageable {
      sort: Sort;
      pageSize: number;
      pageNumber: number;
      offset: number;
      unpaged: boolean;
      paged: boolean;
  }

  export interface Sort2 {
      sorted: boolean;
      unsorted: boolean;
      empty: boolean;
  }

  export interface SuperHumanos {
      content: Content[];
      pageable: Pageable;
      last: boolean;
      totalPages: number;
      totalElements: number;
      sort: Sort2;
      numberOfElements: number;
      first: boolean;
      size: number;
      number: number;
      empty: boolean;
  }

  export interface RootObject {
      superHumanos: SuperHumanos;
  }

}
