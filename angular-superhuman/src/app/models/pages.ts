export class Pages {
  size: number;
  page: number;
  sort: string = "id";
}
