import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from "@angular/common/http";
import { environment } from 'src/environments/environment';
import { Pages } from '../models/pages';

enum EndPoint {
  Superhuman = "superhuman",
}

@Injectable({
  providedIn: 'root'
})
export class SuperhumanService {

  constructor(
    private http: HttpClient
  ) { }

  /**
   * Método que obtiene la url del servidor más el enum del caso de uso
   */
  getUrlService(endPoint: EndPoint) {
    return environment.apiUrl + endPoint;
  }

  listAllSuperHuman(pages: Pages) {
    const params = new HttpParams();
    params.set("sort", pages.sort);
    params.set("size", pages.size.toString());
    params.set("page", pages.page.toString());

    return this.http.get<any>(
      this.getUrlService(EndPoint.Superhuman) + "/list",
      { params: params }
    );
  }

}
